package com.example.foodgotten;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import uk.co.senab.photoview.PhotoViewAttacher;

public class FullPictureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_picture);

        Intent incoming_intent = getIntent();
        //String path = incoming_intent.getStringExtra(C.PHOTOPATH);
        Item item = incoming_intent.getParcelableExtra(C.ITEM);

        PhotoViewAttacher pAttacher;
        ImageView fullImageView = findViewById(R.id.full_picture);
        //fullImageView.setImageBitmap(getFullScalePhoto(path));
        fullImageView.setImageBitmap(item.getFullPic(this));
        pAttacher = new PhotoViewAttacher(fullImageView);
        pAttacher.update();

    }


    public Bitmap getFullScalePhoto(String path){
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path, bmOptions);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        return bitmap;
    }
}
