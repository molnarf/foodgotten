package com.example.foodgotten;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.CompoundButton;

public class SettingsActivity extends AppCompatActivity {

    private CheckBox checkBoxSync;
    private Button btnUpload;
    private Button btnDownload;
    private Button btnSave; // Save button

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        checkBoxSync = findViewById(R.id.checkBoxSync);
        btnUpload = findViewById(R.id.btnUpload);
        btnDownload = findViewById(R.id.btnDownload);
        btnSave = findViewById(R.id.btnSave); // Initialize the Save button

        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        Boolean sync_with_dropbox = preferences.getBoolean(C.SYNC_WITH_DROPBOX, false);
        if (sync_with_dropbox) {
            checkBoxSync.setChecked(true);
        }

        // Set listeners for checkbox changes
        checkBoxSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferences.edit();
                if(isChecked){
                    editor.putBoolean(C.SYNC_WITH_DROPBOX, true);
                }
                else{
                    editor.putBoolean(C.SYNC_WITH_DROPBOX, false);
                }
                editor.apply();
            }
        });

        // Set click listeners for buttons
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, DropboxActivity.class);
                intent.putExtra(C.DROPBOX_ACTION, C.UPLOAD);
                startActivity(intent);
            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, DropboxActivity.class);
                intent.putExtra(C.DROPBOX_ACTION, C.DOWNLOAD);
                startActivity(intent);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(SettingsActivity.this, ScrollingActivity.class);
                    startActivity(intent);
            }
        });

        // You can also handle checkbox state changes as previously demonstrated
    }
}