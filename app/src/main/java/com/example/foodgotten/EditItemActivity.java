package com.example.foodgotten;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import java.util.ArrayList;

public class EditItemActivity extends AddItemActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent incoming_intent = getIntent();
        final ArrayList<Item> items = Utils.getItemlistFromIntent(incoming_intent);
        final int index_to_edit = incoming_intent.getIntExtra(C.INDEX, 0);
        //final Item item_to_edit = items.get(index_to_edit);
        Item item_to_edit = getIntent().getParcelableExtra(C.ITEM);

        // ADD DELETE BUTTON
        LinearLayout verticalView = (LinearLayout) findViewById(R.id.vertical_addItem);
        Button deleteButton = createDeleteButton(items, index_to_edit, item_to_edit);
        verticalView.addView(deleteButton);
        addItemContentToViews(item_to_edit);
        this.photoHandler.currentPhotoPath = item_to_edit.getPhotoPath();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Add created item to list
                Item newItem = getItemFromContext();
                newItem.setCreationtime(item_to_edit.getCreationtime());
                newItem.setDate(item_to_edit.getDate());
                items.set(index_to_edit, newItem);

                // Change Activity
                Intent myIntent = new Intent(EditItemActivity.this, ScrollingActivity.class);
                if(items != null){
                    myIntent.putParcelableArrayListExtra(C.ITEMS, items);
                }

                EditItemActivity.this.startActivity(myIntent);
            }
        });
    }

    public void addItemContentToViews(Item item){
        super.addItemContentToLayout(item);
    }

    public Button createDeleteButton(ArrayList<Item> items, int index_to_edit, Item item_to_edit){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, (int) Utils.convertDpToPixel(40, this));
        Button deleteButton = new Button(this);
        deleteButton.setLayoutParams(layoutParams);
        deleteButton.setBackgroundColor(Color.RED);
        deleteButton.setText("Delete");

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                items.remove(index_to_edit);

                                // Change Activity
                                Intent myIntent = new Intent(EditItemActivity.this, ScrollingActivity.class);
                                if(items != null){
                                    myIntent.putParcelableArrayListExtra(C.ITEMS, items);
                                }
                                EditItemActivity.this.startActivity(myIntent);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("Delete " + item_to_edit.getTitle() + "?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

        return deleteButton;
    }
}
