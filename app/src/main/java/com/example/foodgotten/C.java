package com.example.foodgotten;

import java.io.Serializable;

public class C {

    public static final String ITEM = "ITEM";
    public static final String ITEMS = "ITEMS";
    public static final String INDEX = "INDEX";
    public static final String PHOTOPATH = "PHOTOPATH";


    public static final String TITLE = "TITLE";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String RATING = "RATING";
    public static final String CREATIONTIME = "CREATIONTIME";
    public static final String DATE = "DATE";
    public static final String FOLDERS = "FOLDERS";
    public static final String PRICE = "PRICE";

    public static final String SELECTEDFOLDERS = "SELECTEDFOLDERS";

    public static final String SYNC_WITH_DROPBOX = "SYNC_WITH_DROPBOX";
    public static final String DROPBOX_ACTION = "DROPBOX_ACTION";

    public enum DropboxAction implements Serializable {
        DOWNLOAD,
        UPLOAD,
    }

    public static final String UPLOAD = "UPLOAD";
    public static final String DOWNLOAD = "DOWNLOAD";

//    The file paths for dropbox
    public static final String ITEMFILE_PATH = "/data/user/0/com.example.foodgotten/files/itemfile.txt";
    public static final String FOLDERSFILE_PATH = "/data/user/0/com.example.foodgotten/files/foldersfile.txt";
}
