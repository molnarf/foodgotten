package com.example.foodgotten;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Utils {

    public static ArrayList<Item> getItemlistFromIntent(Intent intent){
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(C.ITEMS)) {
                ArrayList<Item> itemlist = extras.getParcelableArrayList(C.ITEMS);
                return itemlist;
        }
        return null;
    }

    public static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float convertPixelsToDp(float px, Context context){
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static ArrayList<Item> getItemsFromFile(String path, AppCompatActivity activity){
        FileInputStream fis = null;
        ArrayList<Item> items = new ArrayList<>();
        if (!activity.getFileStreamPath(path).exists()){
            return items;
        }
        try {
            fis = activity.openFileInput(path);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String item_string;

            while ((item_string = br.readLine()) != null){
                Item item = itemFromString(item_string);
                items.add(item);
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Item itemFromString(String itemString){
        HashMap<String, String> itemMap = new HashMap<>();
        String[] item_components = itemString.split("\t");
        for(String feature: item_components){
            String[] keyValue = feature.split(":");
            String key = keyValue[0];
            String value = "";
            if(keyValue.length > 1){
                value = keyValue[1];
            }
            itemMap.put(key, value);
        }
        ArrayList<String> folders = new ArrayList<>(Arrays.asList(itemMap.get(C.FOLDERS).split(",")));

        Item item = new Item(itemMap.get(C.TITLE), itemMap.get(C.DESCRIPTION), itemMap.get(C.PHOTOPATH),
                Float.parseFloat(itemMap.get(C.RATING)), Long.parseLong(itemMap.get(C.CREATIONTIME)),
                itemMap.get(C.DATE), folders);

        item.setPrice(Double.parseDouble(itemMap.getOrDefault(C.PRICE, "0.0")));

        return item;
    }

    public static String arrayListToString(ArrayList<String> list){
        String output = "";
        for(String s: list){
            output += s + ", ";
        }
        return output;
    }

    public static ArrayList<String> loadFolders(AppCompatActivity context){
        ArrayList<String> folders = new ArrayList<>();
        File f = new File(context.getApplicationContext().getFilesDir(), ScrollingActivity.FOLDERS_FILE);
        if(!f.exists()){
            return folders;
        }

        FileInputStream fis = null;

        try {
            fis = context.openFileInput(ScrollingActivity.FOLDERS_FILE);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String folder_name;

            while ((folder_name = br.readLine()) != null){
                folders.add(folder_name);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return folders;
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        return bitmap;
    }

    public static void saveFolders(ArrayList<String> folders, AppCompatActivity context){
        if(folders.isEmpty()){
            return;
        }
        String allFolders = TextUtils.join("\n", folders);
        FileOutputStream fos = null;

        try {
            fos = context.openFileOutput(ScrollingActivity.FOLDERS_FILE, context.MODE_PRIVATE);
            fos.write(allFolders.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Intent createIntentWithItems(Intent incoming_intent, AppCompatActivity start_activity, Class goal_activity){
        ArrayList<Item> items = Utils.getItemlistFromIntent(incoming_intent);
        Intent myIntent = new Intent(start_activity, goal_activity);
        if(items != null){
            myIntent.putParcelableArrayListExtra(C.ITEMS, items);
        }

        Bundle extras = incoming_intent.getExtras();
        if (extras.containsKey(C.INDEX)){
            int index = incoming_intent.getIntExtra(C.INDEX, 0);
            myIntent.putExtra(C.INDEX, index);
        }
        if (extras.containsKey(C.ITEM)){
            Item item = incoming_intent.getParcelableExtra(C.ITEM);
            myIntent.putExtra(C.ITEM, item);
        }
        return myIntent;
    }


     public static ArrayList<Item> searchItems(String searchQuery, ArrayList<Item> items) {
         searchQuery = searchQuery.toUpperCase();
        ArrayList<Item> fittingItems = new ArrayList<>();

        // First add all items starting with the search string
        for(Item item: items){
            String title = item.getTitle().toUpperCase();
            if(title.startsWith(searchQuery)){
                fittingItems.add(item);
            }
        }

        // Then add all items containing the search string somewhere
        for(Item item: items){
            String title = item.getTitle().toUpperCase();
            if(!title.startsWith(searchQuery) && title.contains(searchQuery)){
                fittingItems.add(item);
            }
        }
        return fittingItems;
    }





}
