package com.example.foodgotten;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoHandler {

    String currentPhotoPath;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private AppCompatActivity context;

    public PhotoHandler(AppCompatActivity context){
        this.context = context;
    }


    private File createImageFile(){
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            Toast.makeText(context, "Error Occured", Toast.LENGTH_LONG).show();

            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        //Toast.makeText(context, "Saved image to " + currentPhotoPath, Toast.LENGTH_LONG).show();

        return image;
    }


    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            photoFile = createImageFile();
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(context,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //Toast.makeText(context, "URI: " + photoURI.toString(), Toast.LENGTH_LONG).show();

                context.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
            else{
                Toast.makeText(context, "Error Occured", Toast.LENGTH_LONG).show();
            }
        }
    }

    public AppCompatActivity getContext(){
        return this.context;
    }
}
