package com.example.foodgotten;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


// This class is responsible for hiding the Add button when the user is scrolling through the items
// and make it reappear once the scrolling stops
public class ScrollAwareFABBehavior extends FloatingActionButton.Behavior {

    public ScrollAwareFABBehavior(Context context, AttributeSet attrs) {
        super();
    }

    @Override
    public boolean onStartNestedScroll(
            CoordinatorLayout coordinatorLayout,
            FloatingActionButton child,
            View directTargetChild,
            View target,
            int axes,
            int type
    ) {
        return axes == View.SCROLL_AXIS_VERTICAL ||
                super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, axes, type);
    }

    @Override
    public void onNestedScroll(
            CoordinatorLayout coordinatorLayout,
            FloatingActionButton child,
            View target,
            int dxConsumed,
            int dyConsumed,
            int dxUnconsumed,
            int dyUnconsumed,
            int type
    ) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type);
        child.hide();
    }

    @Override
    public void onStopNestedScroll(
            CoordinatorLayout coordinatorLayout,
            FloatingActionButton child,
            View target,
            int type
    ) {
        super.onStopNestedScroll(coordinatorLayout, child, target, type);
        // Show the FAB when the scrolling stops
        child.show();
    }
}
