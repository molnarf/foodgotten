#### Foodgotten

An android app to keep track of food you liked/disliked. 
Were you ever in the situation that you were standing in front of a product in a store that you tried before but have forgotten how good it was?
Foodgotten is meant as a personal reference book for rating food.

<img src="media/foodgotten_screenshot.jpg" width="400">
